import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import 'antd/dist/antd.css';

import { Menu, Icon } from 'antd';

const cerrarSesionUsuario = (cliente, history) => {
    localStorage.removeItem('token', '');
    localStorage.removeItem('id', '');
    
    // Desloguear
    cliente.resetStore();

    history.push('/');
}


const CerrarSesion = ({active_index, history}) => {

        const index = active_index;

       

        return (
            <ApolloConsumer>

            { cliente => {
                 console.log('cliente: ', cliente)
                return (
                    
                    
                    <Menu.Item
                        className={active_index === 3 ? "active simple" : "simple"}
                        onClick={() => cerrarSesionUsuario(cliente, history)}
                        >
                        <Icon type="logout" />
                        <span>Cerrar sesión</span>
                     </Menu.Item> 
                     
                )
            }}

            </ApolloConsumer>
        )
}

 export default withRouter(CerrarSesion);