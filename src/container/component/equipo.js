import React, { Component } from "react";
import 'antd/dist/antd.css';
import { List, Avatar, Tag, Button, } from 'antd';
import Publicprofile from "./../Public-Profile/Public-Profile";
import Filtros from "./filtros";




const data = [
    {
        title: 'Ant Design Title 1',
    },
    {
        title: 'Ant Design Title 2',
    },
    {
        title: 'Ant Design Title 3',
    },
    {
        title: 'Ant Design Title 4',
    },
];



class Team extends Component {
    render() {
        
        return (
            <div style={{textAlign: "left", marginTop: 40 }}>
                <h3>Equipo de trabajo</h3>
                <Filtros />
                <List
                    itemLayout="horizontal"
                    dataSource={data}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                title={<p>Leudy Martes , <Tag color="green">Peluquero</Tag></p>  }
                                description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                            />
                            <Button><Publicprofile /></Button>
                        </List.Item>
                    )}
                />

            </div>
        )

    }
}


export default Team;
