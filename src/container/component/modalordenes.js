import React, { Component } from 'react'
import { Progress, Switch, Card, Icon, Avatar, Button } from 'antd';

const { Meta } = Card;

class Modalordenes extends Component {

  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Card style={{ width: '100%', marginTop: 16, marginBottom: 16 }}>
          <Meta
            title={
              <div>
                <h6>
                Fecha del realizacion del servicio: 
                </h6>
                <p>
                  15/11/2019 a las 15:00
                </p>
              </div>
            }
          />

          <Button
              type="secondary"
              className="btnflex"
              shape="round"
              icon="calendar"
            >
              Añadir a Google Calendar
            </Button>
        </Card>
        
        <h6>Informacón del cliente</h6>
        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            title={this.props.data ? this.props.data.client.nombre : ''}
            description={
              <div>
                <p>
                  {this.props.data ? this.props.data.client.calle : ''}
                  <br />
                  {this.props.data ? this.props.data.client.ciudad : ''}, {this.props.data ? this.props.data.client.provincia : ''}
                  <br />
                  {this.props.data ? this.props.data.client.codigopostal : ''}
                  <br />
                  {this.props.data ? this.props.data.client.telefono : ''}
                </p>
              </div>
            }
          />
        </Card>
        <h6 style={{ marginTop: 16 }}>Informacón del servicio</h6>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={this.props.data && this.props.data.product.fileList.length > 0 ?
              <Avatar src={"http:/localhost:400/assets/images/" + this.props.data.product.fileList[0]} /> :
              <Avatar src=" " />
            }
            title={this.props.data ? this.props.data.product.title : ''}
            description={this.props.data ? "Cantidad: " + this.props.data.cantidad + " " + this.props.data.product.currency : ''}
          />
        </Card>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            description={
              <div>
                <h6>Nota del cliente:</h6>
                <p>{this.props.data ? this.props.data.nota: ''}</p>
              </div>
            }
          />
        </Card>

      </div>
    )
  }
}

export default Modalordenes;
