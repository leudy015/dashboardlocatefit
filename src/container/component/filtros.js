import React, { Component } from "react";
import 'antd/dist/antd.css';
import { Icon, Input, AutoComplete, Col, Row, DatePicker, Radio } from 'antd';

class Filtros extends Component {

    render() {
        return (
            <div style={{textAlign: "left", marginTop: 40, marginBottom:40 }}>
                <Row>
                    <Col span={6}>
                        <p>Buscar</p>
                        <div className="certain-category-search-wrapper" style={{ width: 250 }}>
                            <Input suffix={<Icon type="search" className="certain-category-icon" />} />
                        </div>
                    </Col>
                </Row>
            </div>
        )

    }
}


export default Filtros;
