import React, { Component } from "react";
import { Table, Button, Modal, DatePicker, Form, message, Popover } from "antd";
import { Query, Mutation } from 'react-apollo';
import { useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { PROFESSIONAL_ORDENES_QUERY } from "../../queries";
import { PROFESSIONAL_ORDEN_PROCEED } from "../../mutations";
import TextArea from "antd/lib/input/TextArea";
import Modalordenes from './modalordenes';

const { MonthPicker, RangePicker } = DatePicker;
const { confirm } = Modal;

function range(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}


function disabledDate(current) {
  // Can not select days before today and today
  return current && current < moment().startOf('day');
}

function disabledDateTime() {
  return {
    disabledHours: () => range(0, 24).splice(24, 24),
    disabledMinutes: () => range(60, 60),
  };
}


// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      "selectedRows: ",
      selectedRows
    );
  },
  getCheckboxProps: record => ({
    disabled: record.name === "Disabled User", // Column configuration not to be checked
    name: record.name
  })
};

class TablaOrdenes extends Component {
  constructor(props) {
    super(props);
    console.log('this.props in tableordenes: ', this.props)
    this.state = {
      visible: false,
      visible1: false,
      dataLoading: true,
      formLoading: false,
      currentOrder: null,
      ordenes: []
    };
  }

  refetch = null;
  profesionalId = localStorage.getItem('id');

  componentWillReceiveProps({ dateRange }) {
    console.log('dateRange: ', dateRange)
    if (dateRange && this.refetch) {
      this.setState({dataLoading: true});
      this.refetch({profesional: this.profesionalId, dateRange}).then(res => {
        if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
          this.setState({ ordenes: res.data.getOrdenesByProfessional.list, dataLoading: false });
        }
      });
    }
  }

  showAcceptModal = (e, data) => {
    e.preventDefault();
    this.setState({
      currentOrder: data,
      visible: true,
    });
  };

  showDetailModal = (e, data) => {
    e.preventDefault();
    this.setState({
      currentOrder: data,
      visible1: true,
    });
  };

  handleCancel = () => {
    this.setState({
      currentOrder: null,
      visible: false,
    });
  };

  hideDetailModal = () => {
    this.setState({
      currentOrder: null,
      visible1: false,
    });
  };

  formatResult = data => {
    if (data && data.getOrdenesByProfessional && data.getOrdenesByProfessional.success) {
      this.setState({
        dataLoading: false,
        ordenes: data.getOrdenesByProfessional.list
      });
    }
  }

  showRejectConfirmModal(e, ordenProceed, data) {
    e.preventDefault();
    const that = this;
    confirm({
      title: '¿Esta seguro que deseas rechazar esta orden?',
      content: 'Lamentamos que no haya podido cumplir con esta orden',
      okText: 'Si',
      okType: 'danger',
      cancelText: 'No',
      onOk() {

        return new Promise((resolve, reject) => {

          const formData = {
            ordenId: data.id,
            estado: 'Rechazado',
          }
          ordenProceed({ variables: formData }).then(res => {
            if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
              setTimeout(() => {
                message.success(res.data.ordenProceed.message);
                if (that.refetch) {
                  that.refetch().then(res => {
                    if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                      that.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                    }
                  });
                }
              }, 300)
              resolve();
            }
          }).catch(err => {
            setTimeout(() => {
              message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
            }, 300)
          })
        })
      },

      onCancel() {
        console.log('Cancel');
      },
    });
  }

  finalizar(e, ordenProceed, data) {
    e.preventDefault();

    const formData = {
      ordenId: data.id,
      estado: 'Finalizada',
    }
    ordenProceed({ variables: formData }).then(res => {
      if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
        message.success('La orden ha sido marcada como finalizada éxitosamente');
        if (this.refetch) {
          this.refetch().then(res => {
            if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
              this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
            }
          });
        }
      }
    }).catch(err => {
      message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
    })

  }

  render() {

    const content = (
      <div>
        <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
          {(ordenProceed) => (
            <div className="nooota">
              <Form onSubmit={e => {
                e.preventDefault();
                this.props.form.validateFields((err, values) => {
                  if (!err) {
                    this.setState({ formLoading: true });
                    const formData = {
                      ordenId: this.state.currentOrder.id,
                      estado: 'Aceptado',
                      nota: values.nota
                    }
                    ordenProceed({ variables: formData }).then(res => {
                      if (res && res.data && res.data.ordenProceed && res.data.ordenProceed.success) {
                        this.setState({ visible: false, formLoading: false });
                        message.success(res.data.ordenProceed.message);
                        if (this.refetch) {
                          this.refetch().then(res => {
                            if (res && res.data && res.data.getOrdenesByProfessional && res.data.getOrdenesByProfessional.success) {
                              this.setState({ ordenes: res.data.getOrdenesByProfessional.list });
                            }
                          });
                        }
                      }
                    }).catch(err => {
                      this.setState({ visible: false, formLoading: false });
                      message.error('Algo salió mal. Por favor intente nuevamente en un momento.')
                    })
                  }
                });
              }}>
                <p>Recuerda brindar el mejor servicio a tu cliente</p>

                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.formLoading}
                  className="btnflex"
                  shape="round"
                  icon="check"
                  style={{ marginRight: 10 }}
                >Aceptar orden</Button>

              </Form>
            </div>
          )}
        </Mutation>
      </div>
    );

    const columns = [
      {
        title: "Nº Pedido",
        key: "id",
        render: data => <p onClick={e => this.showDetailModal(e, data)}>{data.id}</p>
      },
      {
        title: "Fecha",
        dataIndex: "created_at",
        render: created_at => new Date(Number(created_at)).toLocaleDateString()
      },
      {
        title: "Estado",
        dataIndex: "estado"
      },
      {
        title: "Total",
        key: "total",
        render: dataIndex => dataIndex.cantidad * dataIndex.product.number
      },
      {
        title: "Acción",
        key: "action",

        render: dataIndex => {
          if (dataIndex && dataIndex.pagoPaypal) {
            return (
              <span className="btnflex">
                <Popover content={content} trigger="click" title="Aceptar orden">
                  {dataIndex && dataIndex.estado === 'Nueva' ?
                    <Button
                      onClick={e => this.showAcceptModal(e, dataIndex)}
                      type="primary"
                      className="btnflex"
                      shape="round"
                      icon="check"
                      style={{ marginRight: 10 }}
                    >
                      Aceptar orden
                  </Button>

                    : ''}
                </Popover>

                {dataIndex && dataIndex.estado === 'Nueva' ?
                  <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                    {(ordenProceed) => (
                      <Button
                        type="danger"
                        className="btnflex"
                        shape="round"
                        icon="close"
                        style={{ marginRight: 10 }}
                        onClick={e => this.showRejectConfirmModal(e, ordenProceed, dataIndex)}
                      >Rechazar</Button>
                    )}
                  </Mutation>
                  : ''}

                {dataIndex && dataIndex.estado === 'Aceptado' ?
                  <Mutation mutation={PROFESSIONAL_ORDEN_PROCEED}>
                    {(ordenProceed) => (
                      <Button
                        type="secondary"
                        className="btnflex"
                        shape="round"
                        icon="like"
                        onClick={e => this.finalizar(e, ordenProceed, dataIndex)}
                        style={{ marginRight: 10 }}
                      >Finalizada</Button>
                    )}
                  </Mutation>
                  : ''}



              </span>
            )
          } else {
            return ("")
          }
        }
      }
    ];

    return (
      <div>
        <Query query={PROFESSIONAL_ORDENES_QUERY} variables={{ profesional: this.profesionalId }} onCompleted={this.formatResult}>
          {({ refetch }) => {
            this.refetch = refetch;
            return (
              <div className="">
                <Table
                  rowKey={orden => orden.id}
                  rowSelection={rowSelection}
                  columns={columns}
                  loading={this.state.dataLoading}
                  dataSource={this.state.ordenes}
                />
              </div>
            )
          }}
        </Query>
        <Modal
          style={{ padding: 20 }}
          footer={null}
          title="Detalles de la orden"
          visible={this.state.visible1}
          onCancel={this.hideDetailModal}
        >
          <Modalordenes data={this.state.currentOrder} />
        </Modal>
      </div>
    );
  }
}

const WrappedTablaOrdenes = Form.create({ name: "TablaOrdenesForm" })(TablaOrdenes);

export default WrappedTablaOrdenes;