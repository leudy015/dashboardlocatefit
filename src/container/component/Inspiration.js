import React, { Component } from "react";
import { Form, Input, Button, message } from 'antd';
import TextArea from "antd/lib/input/TextArea";
import "./../dashboard.css"
import {NUEVO_INSPIRATION } from '../../mutations';
import { Mutation } from 'react-apollo';


class Inspiration extends Component {
    state = { loading: false }

    formSubmit = (e, crearInspiration) => {
        e.preventDefault();
        this.setState({ loading: true });
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const input = {
                    title: values.title,
                    image: values.image,
                    image1: values.image1,
                    image2: values.image2,
                    description: values.description
                };
                crearInspiration({ variables: { input } }).then(res => {
                    if (res) {
                        message.success('Inspiración creada correctamente');
                        this.props.form.resetFields();
                        this.setState({ loading: false });
                    }
                }).catch(err => message.error('Algo salió mal. Por favor intente nuevamente en un momento.'))
            }
        });
    }
    
    render(){
        const { getFieldDecorator } = this.props.form;
        return(
            <Mutation mutation={NUEVO_INSPIRATION}>
                {(crearInspiration) => (
            <div style={{ marginTop: 40 }}>
                <div style={{ textAlign: "left", marginTop: 40, width:"50%", marginLeft: 30}}>
                <Form onSubmit={e => this.formSubmit(e, crearInspiration)}>
                <Form.Item>
                    <h4>Crear Inspiración</h4>
                </Form.Item>
                    <Form.Item>
                    {getFieldDecorator("title", {
                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                    })(<Input  placeholder="Titulo" />)}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator("image", {
                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                    })(<Input placeholder="URL imagen" />)}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator("image1", {
                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                    })(<Input placeholder="URL imagen1" />)}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator("image2", {
                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                    })(<Input placeholder="URL imagen2" />)}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator("description", {
                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                    })(<TextArea placeholder="Descripción" />)}
                    </Form.Item>

                    <Form.Item>
                    <Button loading={this.state.loading} type="primary" htmlType="submit">Crear Inspiración</Button>
                </Form.Item>
                </Form>
                </div>
            </div>
            )}
            </Mutation>
        )
       
    }
}


const WrappedDeposito = Form.create({ name: "inspirationForm" })(Inspiration);
export default WrappedDeposito;
