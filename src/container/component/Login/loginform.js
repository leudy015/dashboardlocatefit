import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import "./../Login/Login.css";
import "./pagelogin.css";
import 'antd/dist/antd.css';
import { Icon, Input, Checkbox, message } from 'antd';





/* import Error from '../Alertas/Error';
 */
import { Mutation } from 'react-apollo';
import { AUTENTICAR_USUARIO } from './../../../mutations';


const initialState = {
    usuario: '',
    email: ''
}

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...initialState
        }
    }

    actualizarState = e => {
        const { name, value } = e.target;

        this.setState({
            [name]: value
        })
    }


    limpiarState = () => {
        this.setState({ ...initialState });
    }

    iniciarSesion = (e, usuarioAutenticar) => {
        e.preventDefault();

        usuarioAutenticar().then(async ({ data: res }) => {
            console.log('in login form data.autenticarUsuario', res.autenticarUsuario)
            if (res.autenticarUsuario.success) {

                localStorage.setItem('token', res.autenticarUsuario.data.token);
                //   localStorage.setItem('id', data.autenticarUsuario.id);

                // ejecutar el query una vez iniciado sesion
                await this.props.refetch();

                // Limpiar state
                this.limpiarState();

                // Redireccionar el contenido
                setTimeout(() => {
                    this.props.history.push('/dashboard');
                }, 500)

            } else {
                message.error(res.autenticarUsuario.message);
            }
        });

    }

    validarForm = () => {



        const { email, password } = this.state;

        const noValido = !email || !password;

        console.log(noValido)
    }



    render() {

        const { email, password } = this.state;

        return (
            <div className="pagelogin">
                <section className="pagelogin1">



                    <div className="register-page-content">


                        <Fragment>

                            <Mutation
                                mutation={AUTENTICAR_USUARIO}
                                variables={{ email, password }}
                            >
                                {(usuarioAutenticar, { loading, error, data }) => {

                                    return (


                                        <div className="containerLogin">
                                            <div className="contLogin">
                                                <h3>Iniciar sesión</h3>
                                                <p>Bienvenido al team de Locatefit</p>

                                                <form
                                                    onSubmit={e => this.iniciarSesion(e, usuarioAutenticar)}
                                                    className="col-md-12"
                                                >

                                                    <div className="form-group">

                                                        <Input
                                                            onChange={this.actualizarState}
                                                            value={email}
                                                            type="text"
                                                            name="email"
                                                            className="form-control"
                                                            placeholder="Email"
                                                        />

                                                    </div>



                                                    <div className="form-group">

                                                        <Input.Password
                                                            onChange={this.actualizarState}
                                                            value={password}
                                                            type="password"
                                                            name="password"
                                                            className="form-control"
                                                            placeholder="Contraseña"
                                                        />
                                                    </div>

                                                    <div className="forgot">
                                                        <Checkbox>Recodar</Checkbox>
                                                        <a className="login-form-forgot" href="https://locatefit.es/recoverpassword">
                                                            ¿Lo olvidaste?
                                                    </a>
                                                    </div>

                                                    <button
                                                        disabled={
                                                            loading || this.validarForm()
                                                        }
                                                        type="submit"
                                                        className="btn btn-primary">
                                                        Iniciar sesión
                                                        </button>



                                                </form>


                                            </div>
                                        </div>



                                    )
                                }}
                            </Mutation>
                        </Fragment>

                    </div>


                    <div className="wave wave1"></div>
                    <div className="wave wave2"></div>
                    <div className="wave wave3"></div>
                    <div className="wave wave4"></div>
                </section>

            </div>


        );
    }
}

export default withRouter(LoginForm);