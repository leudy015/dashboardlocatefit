import React, { Component } from "react";
import 'antd/dist/antd.css';
import { PageHeader, Menu, Dropdown, Icon, Button, Tag, Typography, Row } from 'antd';
import Publicprofile from "./../Public-Profile/Public-Profile";
import Filtros from "./filtros";


const { Paragraph } = Typography;

const menu = (
    <Menu>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
                1st menu item
      </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
                2nd menu item
      </a>
        </Menu.Item>
        <Menu.Item>
            <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
                3rd menu item
      </a>
        </Menu.Item>
    </Menu>
);

const DropdownMenu = () => {
    return (
        <Dropdown key="more" overlay={menu}>
            <Button
                style={{
                    border: 'none',
                    padding: 0,
                }}
            >
                <Icon
                    type="ellipsis"
                    style={{
                        fontSize: 20,
                        verticalAlign: 'top',
                    }}
                />
            </Button>
        </Dropdown>
    );
};


const IconLink = ({ src, text }) => (
    <a
        style={{
            marginRight: 16,
            display: 'flex',
            alignItems: 'center',
        }}
    >
        <img
            style={{
                marginRight: 8,
            }}
            src={src}
            alt="start"
        />
        {text}
    </a>
);

const content = (
    <div className="content">
        <Paragraph>
            Ant Design&#x27;s design team preferred to design with the HSB color model, which makes it
            easier for designers to have a clear psychological expectation of color when adjusting colors,
            as well as facilitate communication in teams.
    </Paragraph>
        <Row className="contentLink" type="flex">
            <IconLink
                src="https://gw.alipayobjects.com/zos/rmsportal/MjEImQtenlyueSmVEfUD.svg"
                text="Compartir"
            />
            <IconLink
                src="https://gw.alipayobjects.com/zos/rmsportal/NbuDUAuBlIApFuDvWiND.svg"
                text={<Publicprofile />}
            />
           
        </Row>
    </div>
);

const Content = ({ children, extraContent }) => {
    return (
        <Row className="content" type="flex">
            <div className="main" style={{ flex: 1 }}>
                {children}
            </div>
            <div
                className="extra"
                style={{
                    marginLeft: 80,
                }}
            >
                {extraContent}
            </div>
        </Row>
    );
};






class Usuarios extends Component {

    render() {

        return (
            <div>
                <h3>Usuarios</h3>
                 <Filtros />
           
            <div style={{ backgroundColor: "#f0f2f5", textAlign: "left", marginTop: 40 }}>
               
                
                <PageHeader
                    title="Leudy Martes"
                    tags={<Tag color="blue">Peluquero</Tag>}
                    extra={[
                        <Button type="danger" key="3">Eliminar usuario</Button>,
                    ]}
                    avatar={{ src: 'https://scontent.fmad7-1.fna.fbcdn.net/v/t1.0-9/57909135_107052430501948_3715130982869762048_n.jpg?_nc_cat=106&_nc_oc=AQl8VuG25ErmFQ0dhmQomrYWK_CT2TEWgPfgbQHcjLRn5KF5035a8HxEeqaz-BiI4RE&_nc_ht=scontent.fmad7-1.fna&oh=0b2d37b7b86f59626840bff05f911555&oe=5DEFFFB3' }}
                    
                >
                    <Content
                      
                    >
                        {content}
                    </Content>
                </PageHeader>
            </div>
            </div>
        )

    }
}


export default Usuarios;
