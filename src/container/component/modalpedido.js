import React, { Component } from 'react'
import { Progress, Switch, Card, Icon, Avatar, Button, Rate } from 'antd';

const { Meta } = Card;

class Modalordenes extends Component {
  render() {
    return (
      <div>
        <div style={{marginBottom: 16}}>
          <p>Progreso de la orden</p>
          <Progress strokeColor={{'0%': '#108ee9','100%': '#87d068'}} percent={50}/>
        </div>

        <Card style={{ width: '100%', marginTop: 16, marginBottom: 16 }}>
          <Meta
            title={
              <div>
                <h6>
                Fecha del realizacion del servicio: 
                </h6>
                <p>
                  15/11/2019 a las 15:00
                </p>
              </div>
            }
          />
        </Card>

        <h6>Informacón del Profesional</h6>
        <Card style={{ width: '100%', marginTop: 16, marginBottom: 16 }}>
          <Meta
            avatar={
              <Avatar src="https://startupxplore.com/uploads/ff8080816d1ea989016d1fc6ceb3012e-large.png" />
            }
            title="Leudy Martes"
            description={
              <div>
                <p>Fontanero</p>
                <Rate disabled defaultValue={5} />
              </div>
            }
          />
        </Card>

        
        <h6>Informacón del cliente</h6>
        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={
              <Avatar src="https://startupxplore.com/uploads/ff8080816d1ea989016d1fc6ceb3012e-large.png" />
            }
            title="Leudy Martes"
            description={
              <div>
                <p>
                  Calle Francisco Sarmiento No. 13 8A
                  <br />
                  Burgos, Burgos
                     <br />
                  09005,<br />
                  689 351 592
                  </p>
              </div>
            }
          />
        </Card>
        <h6 style={{marginTop: 16}}>Informacón del servicio</h6>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            avatar={
              <Avatar src="http://reformasrober.com/wp-content/uploads/2017/05/fontaneros-en-donostia-san-sebastian-reformas-rober.jpg" />
            }
            title="Servicio de fontanería a domicilio"
            description="Cantidad: 4 /Horas"
          />
        </Card>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            description={
              <div>
                <h6>Nota del cliente:</h6>
                <p>Lorem Ipsum es simplemente un texto ficticio de la industria de impresión y composición tipográfica. Lorem Ipsum ha sido el texto ficticio estándar de la industria desde el año 1500,</p>
              </div>
            }
          />
        </Card>

        <Card style={{ width: '100%', marginTop: 16 }}>
          <Meta
            description={
              <div>
                <h6>Nota de profesional:</h6>
                <p>Lorem Ipsum es simplemente un texto ficticio de la industria de impresión y composición tipográfica. Lorem Ipsum ha sido el texto ficticio estándar de la industria desde el año 1500,</p>
              </div>
            }
          />
        </Card>

      </div>
    )
  }
}

export default Modalordenes;
