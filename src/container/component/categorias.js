import React, { Component } from "react";
import { Form, Input, Button, Checkbox } from 'antd';
import TextArea from "antd/lib/input/TextArea";
import "./../dashboard.css"


class Categorías extends Component {
    render(){
        return(
            <div style={{ marginTop: 40 }}>
                <div style={{ textAlign: "left", marginTop: 40, width:"50%"}}>
                <Form.Item>
                    <h4>Crear categoría</h4>
                </Form.Item>
                    <Form.Item>
                        <Input  placeholder="Nombre categoría" />
                    </Form.Item>
                    <Form.Item>
                        <Input placeholder="URL imagen" />
                    </Form.Item>
                    <Form.Item>
                        <TextArea placeholder="Descripción de la  categoría" />
                    </Form.Item>

                    <Form.Item>
                    <Button type="primary">Crear categoría</Button>
                </Form.Item>
                </div>
            </div>
        )
       
    }
}


export default Categorías;
