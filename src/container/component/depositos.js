import React, { Component } from "react";
import { Form, Input, Button, DatePicker, message } from 'antd';
import { Mutation } from 'react-apollo';
import moment from 'moment';
import "./../dashboard.css";
import { NUEVO_USUARIO_DEPOSITO } from '../../mutations';

const dateFormat = 'MM/DD/YYYY'; // compatible con la fecha y hora del servidor para evitar conflictos de API
const defaultDate = '01/01/2020';

class Deposito extends Component {

    state = { loading: false }

    formSubmit = (e, crearUsuarioDeposito) => {
        e.preventDefault();
        this.setState({ loading: true });
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const input = {
                    fecha: moment(values.fecha).format(dateFormat),
                    estado: values.estado,
                    total: values.total,
                    usuarioid: values.usuarioid
                };
                crearUsuarioDeposito({ variables: { input } }).then(res => {
                    if (res.data.crearUsuarioDeposito.success) {
                        message.success(res.data.crearUsuarioDeposito.message);
                        this.props.form.resetFields();
                        this.setState({ loading: false });
                    }
                }).catch(err => message.error('Algo salió mal. Por favor intente nuevamente en un momento.'))
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Mutation mutation={NUEVO_USUARIO_DEPOSITO}>
                {(crearUsuarioDeposito) => (
                    <div style={{ marginTop: 40 }}>
                        <div style={{ textAlign: "left", marginTop: 40, width: "50%" }}>
                            <Form onSubmit={e => this.formSubmit(e, crearUsuarioDeposito)}>
                                <Form.Item>
                                    <h4>Crear deposito</h4>
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("fecha", {
                                        rules: [{ required: true, message: "Este campo es obligatorio" }],
                                        initialValue: moment(defaultDate, dateFormat)
                                    })(<DatePicker format={dateFormat} />)}
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("estado", {
                                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                                    })(<Input placeholder="Estado" />)}
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("total", {
                                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                                    })(<Input placeholder="total" />)}
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("usuarioid", {
                                        rules: [{ required: true, message: "Este campo es obligatorio" }]
                                    })(<Input placeholder="ID del usario" />)}
                                </Form.Item>
                                <Form.Item>
                                    <Button loading={this.state.loading} type="primary" htmlType="submit">Crear deposito</Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                )}
            </Mutation>
        )

    }
}

const WrappedDeposito = Form.create({ name: "depositoForm" })(Deposito);
export default WrappedDeposito;
