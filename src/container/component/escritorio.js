import React, { Component } from "react";
import { Query } from 'react-apollo';
import { USER_DETAIL } from "./../../queries";
import { Statistic, Card, Row, Col, Icon, Tooltip, Progress, Divider, Alert } from 'antd';
import Graficolinea from "./graficolinea";
import Column from "antd/lib/table/Column";
import Graficobarras from "./graficosbarra";


class Escritorio extends Component {
    render() {
        let id = localStorage.getItem('id');
        return (
            <div>

                <div style={{ background: '#ECECEC', padding: '30px' }}>
                    <Row gutter={16}>
                        <Col span={6}>
                            <Card>
                                <Statistic
                                    title="Active"
                                    value={11.28}
                                    precision={2}
                                    valueStyle={{ color: '#3f8600' }}
                                    prefix={<Icon type="arrow-up" />}
                                    suffix="%"
                                />
                                <Statistic title="Total de ventas netas este mes" value={112893} precision={2} />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card>
                                <Statistic
                                    title="Idle"
                                    value={9.3}
                                    precision={2}
                                    valueStyle={{ color: '#cf1322' }}
                                    prefix={<Icon type="arrow-down" />}
                                    suffix="%"
                                />
                                <Statistic title="Pedidos" value={1123873} precision={2} />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card>
                                <Statistic
                                    title="Idle"
                                    value={9.3}
                                    precision={2}
                                    valueStyle={{ color: '#cf1322' }}
                                    prefix={<Icon type="arrow-down" />}
                                    suffix="%"
                                />
                                <Statistic title="Servicios" value={11238536} precision={2} />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card>
                                <Statistic
                                    title="Idle"
                                    value={9.3}
                                    precision={2}
                                    valueStyle={{ color: '#cf1322' }}
                                    prefix={<Icon type="arrow-down" />}
                                    suffix="%"
                                />
                                <Statistic title="Devoluciones" value={118} precision={2} />
                            </Card>
                        </Col>
                    </Row>
                </div>

                <div>
                    <Row>

                        <Col span={10} style={{marginTop: 50, marginRight: 100}}>

                            <Graficolinea />
                           
                        </Col>

                        <Col span={10} style={{marginTop: 50}}>
                            
                        <Graficobarras />
                        </Col>
                    </Row>

                    <Row>
                        <Col span={6}>

                            

                        </Col>

                        <Col span={18}>
                            
                            
                        </Col>


                    </Row>

                </div>











            </div>
        )

    }
}


export default Escritorio;