import React, { Component } from 'react';
import { Layout, Menu, Button, Icon } from 'antd';
import './dashboard.css';
import 'antd/dist/antd.css';
import Escritorio from "./component/escritorio";
import Usuarios from "./component/usuarios"
import Pedidos from "./component/pedidos"
import Servicios from "./component/servicios"
import Team from "./component/equipo"
import Issuse from "./component/issuse"
import Cerrarsesion from "./component/cerrarsesion"
import Estadisticas from './component/estadisticas';
import Categorias from './component/categorias';
import Deposito from './component/depositos';
import Inspiration from './component/Inspiration';


const mainLogo = require("./../../src/logo_main.png");


const { Header, Content, Footer, Sider, Avatar  } = Layout;
const { SubMenu } = Menu;



class Dashboar extends Component {

  state = {
   active_index: 1,
    menuShown: false
  };



  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  changeTab = index => {
    this.setState({ active_index: index });
  };


  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  render() {

    const { active_index } = this.state;
    let id = localStorage.getItem('id');
    

    return (
      <div>
        <Layout style={{ minHeight: '100vh' }}>
          <Sider
          style={{
            textAlign: "left",
            
          }}
           collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
            <div className="logo">
              <a href="/dashboard">
                <img className="logoimg" src={mainLogo} alt="" />
              </a>
            </div>

            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
             
              <Menu.Item
                key="1"
                className={active_index === 1 ? "active simple" : "simple"}
                onClick={() => this.changeTab(1)}
              >
                <Icon type="desktop" />
                <span>Escritorio</span>
              </Menu.Item>

              <Menu.Item
                key="2"
                className={active_index === 2 ? "active simple" : "simple"}
                onClick={() => this.changeTab(2)}
              >
                <Icon type="user" />
                <span>Usuarios</span>

              </Menu.Item>

              <Menu.Item
                key="3"
                className={active_index === 3 ? "active simple" : "simple"}
                onClick={() => this.changeTab(3)}
              >
                <Icon type="file" />
                <span>Pedidos</span>

              </Menu.Item>

              <Menu.Item
                key="5"
                className={active_index === 5 ? "active simple" : "simple"}
                onClick={() => this.changeTab(5)}
              >
                <Icon type="notification" />
                <span>Servicios</span>

              </Menu.Item>

              <Menu.Item
                key="6"
                className={active_index === 6 ? "active simple" : "simple"}
                onClick={() => this.changeTab(6)}
              >
                <Icon type="team" />
                <span>Team</span>

              </Menu.Item>

              <Menu.Item
                key="7"
                className={active_index === 7 ? "active simple" : "simple"}
                onClick={() => this.changeTab(7)}
              >
                <Icon type="warning" />
                <span>Issuse</span>

              </Menu.Item>

              <Menu.Item
                key="9"
                className={active_index === 9 ? "active simple" : "simple"}
                onClick={() => this.changeTab(9)}
              >
                <Icon type="appstore" />
                <span>Categorías</span>

              </Menu.Item>


              <Menu.Item
                key="8"
                className={active_index === 8 ? "active simple" : "simple"}
                onClick={() => this.changeTab(8)}
              >
                <Icon type="area-chart" />
                <span>Estadisticas</span>

              </Menu.Item>

              <Menu.Item
                key="11"
                className={active_index === 11 ? "active simple" : "simple"}
                onClick={() => this.changeTab(11)}
              >
                <Icon type="euro" />
                <span>Deposito</span>

              </Menu.Item>

              <Menu.Item
              key="12"
              className={active_index === 12 ? "active simple" : "simple"}
              onClick={() => this.changeTab(12)}
            >
              <Icon type="picture" />
              <span>Inspiración</span>

            </Menu.Item>


              <Menu.Item key="10">
                  <Cerrarsesion />
              </Menu.Item>

            </Menu>
        
    
          </Sider>
          

          <Layout>

            <Header style={{ textAlign: "right", background: '#fff' }}>
                <h3>Dashboar</h3>
            </Header>
            <Content style={{ margin: '20px 16px' }}>
              <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                {active_index === 1 ? <Escritorio /> : null}
                {active_index === 2 ? <Usuarios /> : null}
                {active_index === 3 ? <Pedidos /> : null}
                {active_index === 5 ? <Servicios /> : null}
                {active_index === 6 ? <Team /> : null}
                {active_index === 7 ? <Issuse /> : null}
                {active_index === 8 ? <Estadisticas /> : null}
                {active_index === 9 ? <Categorias /> : null}
                {active_index === 11 ? <Deposito /> : null}
                {active_index === 12 ? <Inspiration /> : null}
          
          
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Locatefit S.L,  ©2020 Todos los derechos reservados</Footer>
          </Layout>
        </Layout>
      </div>
    );
  }
}


export default Dashboar;