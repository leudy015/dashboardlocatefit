import React from "react";
import "antd/dist/antd.css";
import { Drawer, List, Avatar, Divider, Col, Row } from "antd";

const pStyle = {
  fontSize: 20,
  color: "rgba(0,0,0,0.85)",
  lineHeight: "22px",
  display: "block",
  marginBottom: 16
};

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: "26px",
      marginBottom: 7,
      color: "rgba(0,0,0,0.65)"
    }}
  >
    <p
      style={{
        marginRight: 18,
        marginTop: 12,
        display: "inline-block",
        color: "rgba(0,0,0,0.85)"
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);

class Publicprofile extends React.Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <div>
       <a onClick={this.showDrawer} href="#detalles"> Más información </a> 
        <Drawer
          width={640}
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <Avatar src="https://scontent.fmad7-1.fna.fbcdn.net/v/t1.0-9/57909135_107052430501948_3715130982869762048_n.jpg?_nc_cat=106&_nc_oc=AQl8VuG25ErmFQ0dhmQomrYWK_CT2TEWgPfgbQHcjLRn5KF5035a8HxEeqaz-BiI4RE&_nc_ht=scontent.fmad7-1.fna&oh=0b2d37b7b86f59626840bff05f911555&oe=5DEFFFB3" />
          <p style={{ ...pStyle, marginBottom: 24 }}>Detalles del profesional</p>
          <p style={pStyle}>Información personal</p>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Nombre completo" content="Leudy Martes" />
            </Col>
            <Col span={12}>
              <DescriptionItem
                title="Usuario"
                content="Leudy1521"
              />
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Ciudad" content="Burgos" />
            </Col>
            <Col span={12}>
              <DescriptionItem title="País" content="España 🇪🇸" />
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Cumpleaños" content="15 de Agosto" />
            </Col>
            <Col span={12}>
              <DescriptionItem title="Sitio web" content="leudybarbers.es" />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              
            </Col>
          </Row>
          <Divider />
          <p style={pStyle}>Información profesional</p>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Profesión" content="Peluquero" />
            </Col>
            <Col span={12}>
              <DescriptionItem title="Grado de experiencia" content="Alto (5)" />
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Estudio" content="Bachiller" />
            </Col>
            <Col span={12}>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <DescriptionItem
                title="Descrición"
                content="Data structures, software engineering, operating systems, computer networks, databases, compiler theory, computer architecture, Microcomputer Principle and Interface Technology, Computer English, Java, ASP, etc."
              />
            </Col>
          </Row>
          <Divider />
          <p style={pStyle}>Contacto</p>
          <Row>
            <Col span={12}>
              <DescriptionItem title="Email" content="leudy1521@gmail.com" />
            </Col>
            <Col span={12}>
              <DescriptionItem
                title="Número movil"
                content="+34 689 351 592"
              />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <DescriptionItem
                title="Facebook"
                content={
                  <a href="http://github.com/ant-design/ant-design/">
                    facebook.com/leudy
                  </a>
                }
              />

                <DescriptionItem
                                title="Twitter"
                                content={
                                  <a href="http://github.com/ant-design/ant-design/">
                                    twitter.com/leudy
                                  </a>
                                }
                              />

                <DescriptionItem
                                title="Instagram"
                                content={
                                  <a href="http://github.com/ant-design/ant-design/">
                                    instagram.com/leudy
                                  </a>
                                }
                              />

                <DescriptionItem
                                title="YouTube"
                                content={
                                  <a href="http://github.com/ant-design/ant-design/">
                                    youtube.com/leudy
                                  </a>
                                }
                              />
            </Col>
          </Row>
        </Drawer>
      </div>
    );
  }
}

export default Publicprofile;
