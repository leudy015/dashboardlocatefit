import React from "react";
import { Route, Redirect } from "react-router-dom";
/* import { connect } from "react-redux"; */
// import PropTypes from "prop-types";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem('token') ? (
            <Component {...props} />
      ) : (
       <Redirect 
          to={{
            pathname: "/",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default PrivateRoute;