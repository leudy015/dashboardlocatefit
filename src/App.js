import React from 'react';
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import './App.css';
import PrivateRoute from "./container/PrivateRoute"
import PublicRoute from './container/PublicRuter';
import Session from './../src/container/component/Session';
import Dashboard from "./container/dashboard";
import Loginform from "./container/component/Login/loginform";
import Notfound from './container/component/404';

export * from "react-router";

const App = ({refetch, session}) => {

  const { obtenerUsuario } = session;
  const mensajeBienvenida = (obtenerUsuario) ? `Bienvenido ${obtenerUsuario.usuario}` : ''; {/* <Redirect to='/login' /> */};
          
        return (
        <BrowserRouter>
            <div className="App">
              <Switch>
              <Route exact path="/" render={() => <Loginform refetch={refetch} />} />
                 <PrivateRoute exact path="/dashboard"  component={Dashboard}  />  
                 <Route component={Notfound} />
              </Switch>              
            </div>
        </BrowserRouter>
        )

}

const RootSession = Session(App)

export { RootSession }

